# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class TypeEvent(Event3):
    NAME = "type"

    def perform(self):
        self.add_prop("typed-"+self.object2)
        self.inform("type")

class TypeOnEvent(Event3):
    NAME = "type-on"

    def perform(self):
        if (self.object2=="d34th" or self.object2=="l1f3"):
            self.add_prop("typed-on-"+self.object2)
            self.inform("type-on")
        else :
            self.inform("type-on")

