# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action1,Action3
from mud.events import LookEvent, LookWithEvent

class LookAction(Action1):
    EVENT = LookEvent
    ACTION = "look"

class LookWithAction(Action3):
    EVENT = LookWithEvent
    ACTION = "look-with"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    RESOLVE_OBJECT = "resolve_for_use"